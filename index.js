'use strict';

var http = require('http'),
	https = require('https'),
	url = require('url'),
	isBrowser = !(global && module && module.exports),
	win = isBrowser ? (global.window || {}) : window,
	doc = win.document || {cookie:""},
	Promise = require('bluebird');


function getReqOptsFromUrl(reqUrl, method) {
	var parsedUrl = url.parse(reqUrl);
	var ret = {
		hostname: parsedUrl.hostname,
		port: parsedUrl.port || (/^https/.test(parsedUrl.protocol) ? 443 : win.port),
		path: parsedUrl.path, // path contains query string as well
		method: method || 'GET'
	};
	return ret;
}

function request(reqOpts, reqBody, isHTTPS) {
	var lib = (isHTTPS || reqOpts.port === 443) ? https : http,
		deferred = Promise.defer(),
		xsrfcookie;

	// Always have the headers object
	if (!reqOpts.headers) reqOpts.headers = {};

	// Add a header to denote that we are using XHR in the browser
	if (isBrowser) reqOpts.headers['X-Requested-With'] = "XMLHTTPRequest";

	// If there is an XSRF-TOKEN cookie, set the x-xsrf-header
	// NOTE: this is inspired by angular's excellent XSRF support
	xsrfcookie = /(^|[;\s])?XSRF-TOKEN=([^;\s]+)/.exec(doc.cookie);
	if (xsrfcookie && xsrfcookie.length > 2) {
		reqOpts.headers['x-xsrf-token'] = xsrfcookie[2];
	}

	// Convert reqBody to a string
	if (!!reqBody && typeof reqBody !== 'string') {
		reqBody = JSON.stringify(reqBody);
		reqOpts.headers['content-type'] = 'application/json';
	}

	// Make a request object and return with promise
	var req = lib.request(reqOpts, function (res) {
		var data = "",
			dataType = res.headers['content-type'];

		if (typeof res.setEncoding === 'function') res.setEncoding('utf8');

		res.on("data", function (chunk) {
			if (chunk) data += chunk;
		});
		res.on("end", function (chunk) {
			if (chunk) data += chunk;
			// Post process data if dataType == 'application/json'.
			// NOTE: using regex to cover cases where charset may be appended
			if (/application\/json/.test(dataType)) data = JSON.parse(data);
			// Return raw body as received here
			deferred.resolve({body: data, headers: res.headers});
		});
		res.on("error", function (err) {
			deferred.reject(err);
		});
	});

	if (reqBody) req.write(reqBody);

	req.end();

	// Return a promise to which we can attach the "then" handlers
	return deferred.promise;
}

function get(reqUrl) {
	return request(getReqOptsFromUrl(reqUrl));
}

function del(reqUrl) {
	return request(getReqOptsFromUrl(reqUrl, 'DELETE'));
}

function post(reqUrl, data) {
	return request(getReqOptsFromUrl(reqUrl, 'POST'), data);
}

function put(reqUrl, data) {
	return request(getReqOptsFromUrl(reqUrl, 'PUT'), data);
}


module.exports = {
	get: get,
	post: post,
	del: del,
	put: put
};
