'use strict';

var expect = require('chai').expect,
	app = require('koa')(),
	parse = require('co-body'),
	win = global.window = {document: {cookie: ""}},
	t = require('../index');

describe("relay", function () {

	var url,
		xsrfcookie,
		data = [],
		nextId = 1;

	before(function (done) {
		// Add routes

		// GET /json
		app.use(function* (next) {
			if (this.method !== 'GET' || this.path !== '/json') return yield next;
			this.status = 200;
			this.set("X-XSRF-MATCHES", this.headers['x-xsrf-token'] === xsrfcookie ? "yes" : "no");
			this.body = {message: "Hello World!"};
		});

		// GET /
		app.use(function* (next) {
			if (this.method !== 'GET' || this.path !== '/') return yield next;
			this.status = 200;
			this.set("X-XSRF-MATCHES", this.headers['x-xsrf-token'] === xsrfcookie ? "yes" : "no");
			this.body = "OK";
		});

		// POST|PUT|DELETE /data
		app.use(function* (next) {
			if (!/^\/data/.test(this.path)) return yield next;

			var objId = 0,
				r = /^\/data\/(\d+)/,
				matches = r.exec(this.path),
				obj,
				body;

			if (matches && matches.length > 1 && matches[1]) objId = parseInt(matches[1], 10);

			if (!objId && this.method !== 'POST') {
				return yield next;
			} else if (objId) {
				for (let i = 0, len = data.length; i < len; i++) {
					if (data[i].id !== objId) continue;
					obj = data[i];
					break;
				}
			}

			if (this.method !== 'POST' && !obj) return yield next;

			// Handle DELETE
			if (this.method === 'DELETE') {
				let idx = data.indexOf(obj);
				data.splice(idx, 1);

				this.status = 200;
				this.body = "Deleted " + objId;
				this.set("X-XSRF-MATCHES", this.headers['x-xsrf-token'] === xsrfcookie ? "yes" : "no");
				return;
			}

			body = yield parse(this);
			if (!body) return yield next;

			if (this.method === 'POST') {
				body.id = nextId;
				data.push(body);
				nextId++;

				this.status = 200;
				this.body = body;
				this.set("X-XSRF-MATCHES", this.headers['x-xsrf-token'] === xsrfcookie ? "yes" : "no");
				return;
			}

			// Update the object
			Object.keys(body).forEach(function (k) {
				if (k === 'id') return;
				obj[k] = body[k];
			});
			this.status = 200;
			this.body = obj;
			this.set("X-XSRF-MATCHES", this.headers['x-xsrf-token'] === xsrfcookie ? "yes" : "no");
		});

		// Start the app and get an address for it.
		var addr = app.listen().address(),
			hostname = addr.address;
		// NOTE: For IPv6, host needs to be wrapped in []
		if (addr.family = 'IPv6') {
			hostname = '[' + hostname + ']';
		}
		url = 'http://' + hostname + ':' + addr.port;

		done();
	});

	describe("GET", function () {

		beforeEach(function (done) {
			win.document.cookie = xsrfcookie = "";
			done();
		});

		it("/ works", function (done) {

			xsrfcookie = "anxsrfcookie";
			win.document.cookie = 'XSRF-TOKEN=' + xsrfcookie + ';';

			t.get(url + "/").then(
				function (res) {
					expect(res.body).to.equal("OK");
					expect(res.headers['content-type']).to.match(/text\/plain/);
					expect(res.headers['x-xsrf-matches']).to.equal("yes");
					done();
				},
				done
			);
		});

		it("/json works", function (done) {

			win.document.cookie = 'XSRF-TOKEN=invalid;';

			t.get(url + "/json").then(
				function (res) {
					expect(typeof res.body).to.equal("object");
					expect(res.body.message).to.equal("Hello World!");
					expect(res.headers['content-type']).to.match(/application\/json/);
					expect(res.headers['x-xsrf-matches']).to.equal("no");
					done();
				},
				done
			);
		});
	});

	describe("POST", function () {

		beforeEach(function (done) {
			win.document.cookie = xsrfcookie = "";
			data = [];
			nextId = 1;
			done();
		});

		it("/data works", function (done) {

			xsrfcookie = "anxsrfcookie";
			win.document.cookie = '...; XSRF-TOKEN=' + xsrfcookie + '; ...';

			t.post(url + "/data", {num: Math.random()}).then(
				function (res) {
					expect(typeof res.body).to.equal("object");
					expect(res.body.id).to.equal(nextId - 1);
					expect(res.body.num).to.be.ok;
					expect(res.headers['content-type']).to.match(/application\/json/);
					expect(res.headers['x-xsrf-matches']).to.equal("yes");
					done();
				},
				done
			);
		});
	});

	describe("PUT", function () {

		beforeEach(function (done) {
			win.document.cookie = xsrfcookie = "";
			data = [{num: Math.random(), id: 1}];
			nextId = 2;
			done();
		});

		it("/data/1 works", function (done) {

			xsrfcookie = "anxsrfcookie";
			win.document.cookie = '...; XSRF-TOKEN=' + xsrfcookie + '; ...';

			var oldNum = data[0].num;

			t.put(url + "/data/1", {num: Math.random()}).then(
				function (res) {
					expect(typeof res.body).to.equal("object");
					expect(res.body.id).to.equal(nextId - 1);
					expect(res.body.num).to.equal(data[0].num);
					expect(res.body.num).to.not.equal(oldNum);
					expect(res.headers['content-type']).to.match(/application\/json/);
					expect(res.headers['x-xsrf-matches']).to.equal("yes");
					done();
				},
				done
			);
		});
	});

	describe("DELETE", function () {
		beforeEach(function (done) {
			win.document.cookie = xsrfcookie = "";
			data = [{num: Math.random(), id: 1}];
			nextId = 2;
			done();
		});

		it("/data/1 works", function (done) {

			xsrfcookie = "anxsrfcookie";
			win.document.cookie = '...; XSRF-TOKEN=' + xsrfcookie + '; ...';

			t.del(url + "/data/1").then(
				function (res) {
					expect(res.body).to.equal("Deleted 1");
					expect(data.length).to.equal(0);
					expect(res.headers['content-type']).to.match(/text\/plain/);
					expect(res.headers['x-xsrf-matches']).to.equal("yes");
					done();
				},
				done
			);
		});
	});

});
